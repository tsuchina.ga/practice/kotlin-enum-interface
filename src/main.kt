
fun main(args: Array<String>) {
    println("こんにちわーるど")

    println("----- SALT -----")
    println(FooEnum.SALT.enum2Api())
    println(FooEnum.SALT.enum2Db())
    println(BarEnum.SALT.enum2Api())
    println(BarEnum.SALT.enum2Db())

    println("----- SUGAR -----")
    println(FooEnum.SUGAR.enum2Api())
    println(FooEnum.SUGAR.enum2Db())
    println(BarEnum.SUGAR.enum2Api())
    println(BarEnum.SUGAR.enum2Db())

    println("----- VINEGAR -----")
    println(FooEnum.VINEGAR.enum2Api())
    println(FooEnum.VINEGAR.enum2Db())
    println(BarEnum.VINEGAR.enum2Api())
    println(BarEnum.VINEGAR.enum2Db())

    println("----- db: 1 -----")
    println(FooEnum.db2Enum(1))
    println(FooEnum.api2Enum(111))
    println(BarEnum.db2Enum(1))
    println(BarEnum.api2Enum("salt"))

    println("----- db: 2 -----")
    println(FooEnum.db2Enum(2))
    println(FooEnum.api2Enum(222))
    println(BarEnum.db2Enum(2))
    println(BarEnum.api2Enum("sugar"))

    println("----- db: 3 -----")
    println(FooEnum.db2Enum(3))
    println(FooEnum.api2Enum(333))
    println(BarEnum.db2Enum(3))
    println(BarEnum.api2Enum("vinegar"))

    println("----- interface -----")
    listOf(
        Data(FooEnum.SALT),
        Data(FooEnum.SUGAR),
        Data(FooEnum.VINEGAR),
        Data(BarEnum.SALT),
        Data(BarEnum.SUGAR),
        Data(BarEnum.VINEGAR)).forEach {
        println(it.enum::class)
        println(it.enum.enum2Api())
        println(it.enum.enum2Db())
    }

}

data class Data(
    val enum: DbApiEnum
)

interface DbApiEnum {
    fun enum2Api(): Any
    fun enum2Db(): Any
}

enum class BaseEnum(val db: Int) {
    SALT(1),

    SUGAR(2),

    VINEGAR(3)
}

enum class FooEnum(private val db: Int, private val api: Int) : DbApiEnum {

    SALT(BaseEnum.SALT.db, 111),

    SUGAR(BaseEnum.SUGAR.db, 222),

    VINEGAR(BaseEnum.VINEGAR.db, 333);

    override fun enum2Api(): Any = api
    override fun enum2Db(): Any = db

    companion object {
        fun api2Enum(v: Any): DbApiEnum? = when(v) {
            111 -> FooEnum.SALT
            222 -> FooEnum.SUGAR
            333 -> FooEnum.VINEGAR
            else -> null
        }
        fun db2Enum(v: Any): DbApiEnum? = when(v) {
            BaseEnum.SALT.db -> FooEnum.SALT
            BaseEnum.SUGAR.db -> FooEnum.SUGAR
            BaseEnum.VINEGAR.db -> FooEnum.VINEGAR
            else -> null
        }
    }
}

enum class BarEnum(private val db: Int, private val api: String) : DbApiEnum {

    SALT(BaseEnum.SALT.db, "salt"),

    SUGAR(BaseEnum.SUGAR.db, "sugar"),

    VINEGAR(BaseEnum.VINEGAR.db, "vinegar");

    override fun enum2Api(): Any = api
    override fun enum2Db(): Any = db

    companion object {

        fun api2Enum(v: Any): DbApiEnum? = when(v) {
            "salt" -> FooEnum.SALT
            "sugar" -> FooEnum.SUGAR
            "vinegar" -> FooEnum.VINEGAR
            else -> null
        }
        fun db2Enum(v: Any): DbApiEnum? = when(v) {
            BaseEnum.SALT.db -> FooEnum.SALT
            BaseEnum.SUGAR.db -> FooEnum.SUGAR
            BaseEnum.VINEGAR.db -> FooEnum.VINEGAR
            else -> null
        }
    }
}

